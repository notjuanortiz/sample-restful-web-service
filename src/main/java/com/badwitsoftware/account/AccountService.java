package com.badwitsoftware.account;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountService {

	@Autowired
	private AccountRepository accountRepository;

	/**
	 * @param account
	 * @return
	 */
	public Account save(Account account) {
		return accountRepository.save(account);
	}
	
	public void deleteById(Long id) {
		accountRepository.deleteById(id);
	}

	/**
	 * @param id
	 * @return
	 * @throws AccountNotFoundException
	 * 
	 */
	public Account findById(Long id) {
		Optional<Account> account = accountRepository.findById(id);

		if (!account.isPresent()) {
			throw new AccountNotFoundException("Unable to find account with id " + id);
		}
		return account.get();

	}

	/**
	 * @param id
	 * @return
	 */
	public Optional<Account> findOptionalById(Long id) {
		return accountRepository.findById(id);
	}

	/**
	 * @param username
	 * @return
	 * @throws AccountNotFoundException
	 * 
	 */
	public Account findByUsername(String username) {
		Account account = accountRepository.findByUsername(username);

		if (account == null) {
			throw new AccountNotFoundException("Unable to find account with username " + username);
		}
		return account;
	}

	/**
	 * @param email
	 * @return
	 * @throws AccountNotFoundException
	 * 
	 */
	public Account findByEmail(String email) {
		Account account = accountRepository.findByEmail(email);

		if (account == null) {
			throw new AccountNotFoundException("Unable to find account with email address " + email);
		}
		return account;
	}

	/**
	 * @return
	 * @throws AccountNotFoundException
	 */
	public List<Account> findAll() {
		List<Account> accounts = accountRepository.findAll();

		if (accounts == null | accounts.isEmpty()) {
			throw new AccountNotFoundException("Unable to find any accounts.");
		}

		return accounts;
	}

}
